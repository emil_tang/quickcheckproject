module TestProject.Properties

open System
open System.Collections.Generic
open QuikGraph
open QuikGraph.Algorithms.TopologicalSort
open QuikGraph.Algorithms
open FsCheck

type TopologicalSort = 
    static member ``All Elements Must Also Exist In Graph`` (g:AdjacencyGraph<int,SEquatableEdge<int>>) =
        let a = g.TopologicalSort()
        g.Vertices |> Seq.forall (fun x -> Seq.contains x a) 
        |@ sprintf "sorted = %A, original: vertices = %A, edges = %A" a g.Vertices g.Edges

    /// <summary>
    /// Check that all sorted vertices has an edge to another vertex further in the list
    /// </summary>
    static member ``All Elements MustHave An Edge To A Previous Vertex`` (g:AdjacencyGraph<int,SEquatableEdge<int>>) =
        let rec check (edges:SEquatableEdge<int> list) vertices =
            match vertices with
            | [] | _ :: [] -> true
            | vertex :: tail ->
                edges
                |> List.filter (fun edge -> edge.Target = vertex)
                |> List.forall (fun edge -> List.contains edge.Source tail)
                && check edges tail
        let s = g.TopologicalSort()
        s |> List.ofSeq |> List.rev |> check (List.ofSeq g.Edges)
        |@ sprintf "sorted = %A, original: vertices = %A, edges = %A" s g.Vertices g.Edges

type MinimalSpanningTree =
    
   static member ``Kruskal Idempotence`` (g:UndirectedGraph<int,SEquatableEdge<int>>) =
        let f = Func<SEquatableEdge<int>,float> (fun x -> float x.Source)
        let a = g.MinimumSpanningTreeKruskal(f)
        let b = a.ToUndirectedGraph().MinimumSpanningTreeKruskal(f)
        let alist = List.ofSeq a 
        let blist =  List.ofSeq b 
        alist =  blist
        |@ sprintf "alist = %A, blist = %A" alist blist
    
    static member ``Kruskal Idempotence Sort`` (g:UndirectedGraph<int,SEquatableEdge<int>>) =
        let f = Func<SEquatableEdge<int>,float> (fun x -> float x.Source)
        let a = g.MinimumSpanningTreeKruskal(f)
        let b = a.ToUndirectedGraph().MinimumSpanningTreeKruskal(f)
        let alist = List.ofSeq a |> List.sortBy (fun x -> x.ToString()) 
        let blist =  List.ofSeq b |> List.sortBy (fun x -> x.ToString()) 
        alist =  blist
        |@ sprintf "alist = %A, blist = %A" alist blist
        
    static member ``Kruskal Idempotence Two Call`` (g:UndirectedGraph<int,SEquatableEdge<int>>) =
        let f = Func<SEquatableEdge<int>,float> (fun x -> float x.Source)
        let a = g.MinimumSpanningTreeKruskal(f)
        let b = g.MinimumSpanningTreeKruskal(f)
        let alist = List.ofSeq a
        let blist =  List.ofSeq b
        alist =  blist
        |@ sprintf "alist = %A, blist = %A" alist blist
        
    static member ``Prim Idempotence`` (g:UndirectedGraph<int,SEquatableEdge<int>>) =
        let f = Func<SEquatableEdge<int>,float> (fun x -> float x.Source)
        let a = g.MinimumSpanningTreePrim(f)
        let b = a.ToUndirectedGraph().MinimumSpanningTreePrim(f)
        let alist = List.ofSeq a |> List.sortBy (fun x -> x.ToString()) 
        let blist =  List.ofSeq b |> List.sortBy (fun x -> x.ToString())
        alist =  blist
        |@ sprintf "alist = %A, blist = %A" alist blist
        
     /// <summary>
     /// Check the implementation of Kruskal and Prims Minimal Spannning Tree Algorithm against each other
     /// </summary>
    static member ``Compare Kruskal Prim`` (g:UndirectedGraph<int,SEquatableEdge<int>>) =
        let weights = Func<SEquatableEdge<int>,float> (fun x -> float x.Source)
        let kruskal = HashSet (g.MinimumSpanningTreeKruskal(weights))
        let prim = HashSet (g.MinimumSpanningTreePrim(weights))
        kruskal.SetEquals prim 
        |@ sprintf "kruskal = %A, prim = %A" kruskal prim
        
    static member ``Compare sum Kruskal Prim`` (g:UndirectedGraph<int,SEquatableEdge<int>>) =
        let func (x:SEquatableEdge<int>) = float x.Source
        let weights = Func<SEquatableEdge<int>,float> func
        let kruskal = List.ofSeq (g.MinimumSpanningTreeKruskal(weights))
        let prim = List.ofSeq (g.MinimumSpanningTreePrim(weights))
        let kruskal_sum = kruskal |> List.map func |> List.sum
        let prim_sum = prim |> List.map func |> List.sum
        kruskal_sum = prim_sum
        |@ sprintf "kruskal = %A, sum = %f, prim = %A, sum = %f" kruskal kruskal_sum prim prim_sum
    