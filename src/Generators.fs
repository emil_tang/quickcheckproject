module TestProject.Generators

open System
open FsCheck
open QuikGraph

// Gen.choose(Int32.MinValue, Int32.MaxValue) fails with DivideByZeroException
// is apparently fixed in the 3.0 preview version which has been in "development" for 3 years...
// Gen.choose(Int32.MinValue, Int32.MaxValue -1) does not fail but only generates Int32.MinValue
// Maybe is should test this library instead. Testception.

// TODO: Fix is DoNotSize
let uniformIntGen = Arb.generate<DoNotSize<int>> |> Gen.map(fun x -> DoNotSize.Unwrap x)

let chooseInt = Gen.frequency [
    (85, Gen.choose(-255, 255))
    (10, uniformIntGen )
    (5, Gen.elements [ Int32.MaxValue; Int32.MinValue; -1; 0; 1; ])
]

let chooseFromList xs = gen {
        let! i = Gen.choose (0, List.length xs - 1) 
        return List.item i xs
    }

let chooseFromIntPairList xs =
    // stupid hack to fix empty list
    // should not affect the model because of preconditions
    if List.length xs = 0
    then Gen.constant (0, 0)
    else gen {
        let! i = Gen.choose (0, List.length xs - 1) 
        return List.item i xs
    }
    
let chooseFromIntList xs =
    // stupid hack to fix empty list
    // should not affect the model because of preconditions
    if List.length xs = 0
    then Gen.constant 0
    else gen {
        let! i = Gen.choose (0, List.length xs - 1) 
        return List.item i xs
    }

let chooseUniqueFromIntList xs =
    chooseInt |> Gen.filter(fun x -> not (List.contains x xs))

type Graph =
    static member AcyclicDirected = {
        new Arbitrary<AdjacencyGraph<int,SEquatableEdge<int>>>() with
            override x.Generator =
                let newEdge tar s = Gen.map (fun src -> (src, tar)) (Gen.elements s) 
                let gen vertices = 
                    let rec loop vertices state acc =
                            match vertices, state with
                            | [], _ -> acc
                            | a :: b :: tail, state when state = [] ->
                                loop tail (a :: b :: state) (Gen.constant (a, b) :: acc)
                            | head :: tail, state ->
                                loop tail (head :: state) (newEdge head state :: newEdge head state :: acc)
                    loop vertices [] [] |> Gen.sequence 
                let initGraph xs =
                        let es = List.map (fun (x, y) -> SEquatableEdge(x, y)) xs
                        let g = AdjacencyGraph<int,SEquatableEdge<int>>()
                        g.AddVerticesAndEdgeRange(es) |> ignore
                        g
                Gen.listOf uniformIntGen
                |> Gen.map List.distinct
                |> Gen.filter (fun x -> List.length x <> 1)
                >>= gen
                |> Gen.map initGraph 
                
            override x.Shrinker t =
                if t.VertexCount = 0 || t.EdgeCount =0
                then Seq.empty
                else
                    let a = t.Clone()
                    a.Edges |> Seq.head |> a.RemoveEdge |> ignore
                    let b = t.Clone()
                    b.Vertices |> Seq.head |> b.RemoveVertex |> ignore
                    seq [a; b;]
    }
    
    static member Undirected = {
        new Arbitrary<UndirectedGraph<int,SEquatableEdge<int>>>() with
            override x.Generator =
                let initGraph xs =
                        let es = List.map (fun (x,y ) -> SEquatableEdge(x,y)) xs
                        let g = UndirectedGraph<int,SEquatableEdge<int>>()
                        g.AddVerticesAndEdgeRange(es) |> ignore
                        g
                let pairs xs = xs |> Gen.shuffle |> Gen.map List.ofArray |> Gen.map List.pairwise
                let gen xs = Gen.map3 (fun x y z -> x @ y @ z) (pairs xs) (pairs xs) (pairs xs)
                Gen.listOf uniformIntGen >>= gen |> Gen.map initGraph
            override x.Shrinker t =
                if t.VertexCount = 0 || t.EdgeCount = 0
                then Seq.empty
                else
                    let a = t.Clone()
                    a.Edges |> Seq.head |> a.RemoveEdge |> ignore
                    let b = t.Clone()
                    b.Vertices |> Seq.head |> b.RemoveVertex |> ignore
                    seq [a; b;]
                    
    }