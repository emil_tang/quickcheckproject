module TestProject.Test

open Microsoft.VisualStudio.TestTools.UnitTesting
open TestProject
open FsCheck
open FsCheck.Experimental
open System.IO
open QuikGraph.Serialization

[<TestClass>]
type ``Property Tests``() =
    
    [<TestMethod>]
    member __.``State Machine`` () =
        Check.One ({Config.Quick with MaxTest = 10000;}, Spec.create |> StateMachine.toProperty)
  
    [<TestMethod>]
    member __.``Test Topological Sort``() =
        Arb.register<Generators.Graph>() |> ignore
        Check.All ({ Config.Quick with EndSize = 100; MaxTest = 10000}, typeof<Properties.TopologicalSort>)
    
    [<TestMethod>]
    member __.``Test Minimal Spanning Tree``() =
        Arb.register<Generators.Graph>() |> ignore
        Check.All ({ Config.Quick with EndSize = 100; MaxTest = 10000}, typeof<Properties.MinimalSpanningTree>)
    
    [<TestMethod>]
    member __.``Sample Integers``() =
        let g = Generators.chooseInt |> Gen.sample 10 10000
        let s = g |> Seq.map (fun i -> i.ToString())
        File.WriteAllLines ("int.txt", s)
        
    [<TestMethod>]
    member __.``Sample Undirected Graph``() =
        let g = Generators.Graph.Undirected.Generator |> Gen.sample 50 1 |> Seq.head
        g.SerializeToGraphML "graph.graphml" 

    [<TestMethod>]
    member __.``Sample Directed Graph``() =
        let g = Generators.Graph.AcyclicDirected.Generator |> Gen.sample 50 1 |> Seq.head
        g.SerializeToGraphML "graph.graphml" 