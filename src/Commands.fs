module Testproject.Commands

open QuikGraph
open FsCheck
open FsCheck.Experimental
open TestProject
open TestProject.Generators
open Testproject

let genAddVertex model =  
    let addVertex v = {
        new Operation<AdjacencyGraph<int, SEquatableEdge<int>>, Graph.Graph>() with
            member __.Run model = Graph.addVertex v model
            member __.Check (actual, model) =
                let res = actual.AddVertex(v)
                res
                |@ sprintf "addVertex: vertex %i, added = %b, model = (%A,%A), actual (%A,%A)"
                       v res (Graph.Vertices model) (Graph.Edges model) (actual.Vertices) (actual.Edges)
    }
    // Chance to generate an already existing vertex is high with small ints
    // Instead choose a new int that is not already in use
    Generators.chooseUniqueFromIntList (Graph.Vertices model)
    |> Gen.map(fun x -> addVertex x)
                   
let genAddEdge model =
    let addEdge x y = {
        new Operation<AdjacencyGraph<int, SEquatableEdge<int>>, Graph.Graph>() with
            override __.Pre model =
                // The shrinker does not work properly without these extra checks
                Graph.countVertices model >= 2
                && Graph.containsVertex x model
                && Graph.containsVertex y model
            member __.Run model = Graph.addEdge (x, y) model
            member __.Check (actual, model) =
                let res = actual.AddEdge(SEquatableEdge<int>(x, y))
                res
                |@ sprintf "addEdge: edge (%i, %i), added = %b, model = (%A,%A), actual (%A,%A)"
                        x y res (Graph.Vertices model) (Graph.Edges model) (actual.Vertices) (actual.Edges)
    }
    Generators.chooseFromIntList (Graph.Vertices model)
    |> Gen.two |> Gen.map (fun (x, y) -> addEdge x y)
        
let genRemoveVertex model =     
    let removeVertex x = {
        new Operation<AdjacencyGraph<int,SEquatableEdge<int>>, Graph.Graph>() with
            override __.Pre model = Graph.countVertices model >= 1
                                    && Graph.containsVertex x model
            member __.Run model = Graph.removeVertex x model
            member __.Check (actual, model) =
                let res = actual.RemoveVertex x
                res
                |@ sprintf "removeVertex: removing = %i, removed = %b, model = (%A,%A), actual (%A,%A)"
                        x res (Graph.Vertices model) (Graph.Edges model) (actual.Vertices) (actual.Edges)
    }                    
    Generators.chooseFromIntList (Graph.Vertices model)
    |> Gen.map (fun x -> removeVertex x)
        
let genRemoveEdge model =     
    let removeEdge x y = {
        new Operation<AdjacencyGraph<int,SEquatableEdge<int>>, Graph.Graph>() with
            override __.Pre model = Graph.countEdges model >= 1
                                    && Graph.containsVertex x model
                                    && Graph.containsVertex y model
            member __.Run model = Graph.removeEdge (x, y) model 
            member __.Check (actual, model) =
                let res = actual.RemoveEdge(SEquatableEdge<int>(x, y))
                res
                |@ sprintf "removeEdge: removing (%i, %i), removed = %b, model =  (%A,%A), actual (%A,%A)"
                        x y res (Graph.Vertices model) (Graph.Edges model) (actual.Vertices) (actual.Edges)
    }
    Generators.chooseFromIntPairList (Graph.Edges model)
    |> Gen.map (fun (x, y) -> removeEdge x y)
     
let genCountEdges () =
    let countEdges = {
        new Operation<AdjacencyGraph<int,SEquatableEdge<int>>, Graph.Graph>() with
            member __.Run model = model
            member __.Check (actual, model) =
                let nModel = Graph.countEdges model
                let nActual = actual.EdgeCount
                nModel = nActual
                |@ sprintf "countEdges: n actual = %i, n model = %i, model =  (%A,%A), actual (%A,%A)"
                       nActual nModel (Graph.Vertices model) (Graph.Edges model) (actual.Vertices) (actual.Edges)
    }
    Gen.constant countEdges
        
let genCountVertices () =
    let countVertices= {
        new Operation<AdjacencyGraph<int,SEquatableEdge<int>>, Graph.Graph>() with
            member __.Run model = model
            member __.Check (actual, model) =
                let nModel = Graph.countVertices model
                let nActual = actual.VertexCount
                nModel = nActual
                |@ sprintf "countVertices: n actual = %i, n model = %i, model =  (%A,%A), actual (%A,%A)"
                       nActual nModel (Graph.Vertices model) (Graph.Edges model) (actual.Vertices) (actual.Edges)
    }
    Gen.constant countVertices

let genContainsVertex (model:Graph.Graph) =
    let containsVertex x = {
        new Operation<AdjacencyGraph<int,SEquatableEdge<int>>, Graph.Graph>() with
            member __.Pre model = Graph.countVertices model >= 1 && Graph.containsVertex x model
            member __.Run model = model
            member __.Check (actual, model) =
                let res = actual.ContainsVertex(x)
                res
                |@ sprintf "containsVertex: vertex = %i, contains = %b, model =  (%A,%A), actual (%A,%A)"
                       x res (Graph.Vertices model) (Graph.Edges model) (actual.Vertices) (actual.Edges)
    }
    Generators.chooseFromIntList (Graph.Vertices model)
    |> Gen.map (fun x -> containsVertex x)
        
let genContainsEdge model =
    let containsEdge x y = {
        new Operation<AdjacencyGraph<int,SEquatableEdge<int>>, Graph.Graph>() with
            member __.Pre model = Graph.countEdges model >= 1 && Graph.containsEdge (x, y) model
            member __.Run model = model
            member __.Check (actual, model) =
                let res = actual.ContainsEdge(SEquatableEdge<int>(x, y))
                res
                |@ sprintf "containsEdge: edge = (%i, %i), contains = %b, model =  (%A,%A), actual (%A,%A)"
                       x y res (Graph.Vertices model) (Graph.Edges model) (actual.Vertices) (actual.Edges) 
    }
    Generators.chooseFromIntPairList (Graph.Edges model)
    |> Gen.map (fun (x, y) -> containsEdge x y)
