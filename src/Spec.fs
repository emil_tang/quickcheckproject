module TestProject.Spec

open FsCheck
open FsCheck.Experimental
open QuikGraph
open Testproject

let private init () = {
    // Use SEquatableEdge instead of SEdge
    // Since SEdge compares on object memory address and SEquatableEdge compares values
    new Setup<AdjacencyGraph<int,SEquatableEdge<int>>, Graph.Graph>() with
        member __.Actual() = AdjacencyGraph()
        member __.Model() = Graph.init ()
}
let create = {
    new Machine<AdjacencyGraph<int, SEquatableEdge<int>>, Graph.Graph>() with
        member __.Setup = Gen.constant (init ()) |> Arb.fromGen
        member __.Next model = Gen.frequency [
              (20, Commands.genAddVertex model)
              (20, Commands.genAddEdge model)
              (10, Commands.genRemoveEdge model) 
              (10, Commands.genRemoveVertex model)
              (5, Commands.genCountEdges ())
              (5, Commands.genCountVertices ())
              (15, Commands.genContainsEdge model)
              (15, Commands.genContainsVertex model)
        ]
}
