module Testproject.Graph 

open System 

let private removeFirst predicate list =
    let rec loop acc = function
        | [] -> List.rev acc
        | x :: xs when predicate x -> (List.rev acc) @ xs
        | x :: xs -> loop (x :: acc) xs
    loop [] list

type Edge = int * int 
type Vertex = int
type Graph = Tuple<List<Vertex>, List<Edge>>

let init () :Graph = ([], [])
let addVertex (vertex:Vertex) (vertices, edges) :Graph = (vertex :: vertices, edges)

// Also remove all edge to or from vertex
let removeVertex (vertex:Vertex) (vertices, edges) :Graph = (
    List.filter (fun v -> v <> vertex) vertices,
    List.filter (fun (x, y) -> x <> vertex && y <> vertex) edges
)
let containsVertex (vertex:Vertex) (vertices, _) :Boolean = List.contains vertex vertices
let countVertices (vertices, _) :int = List.length vertices
let Vertices (vertices, _) :List<Vertex> = vertices

let addEdge (edge:Edge) (vertices, edges) :Graph = (vertices, edge :: edges)

// only remove first edge match
let removeEdge (edge:Edge) (vertices, edges) = (vertices, removeFirst (fun e -> e = edge) edges)

// Faulty implementation, activate for bug
//let removeEdge (edge:Edge) (vertices, edges) = (vertices, List.filter (fun e -> e <> edge) edges)

let containsEdge (edge:Edge) (_, edges) = List.contains edge edges
let countEdges (_, edges) :int = List.length edges
let Edges (_, edges) :List<Edge> = edges
  
      
      
      
